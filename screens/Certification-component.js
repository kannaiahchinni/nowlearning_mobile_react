import React, {Component} from 'react';
import {View, Text, SafeAreaView, ScrollView} from 'react-native';
import Grid from '../components/Grid';

export default class Certification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      class: {
        type: 'certification',
        role_select_target: 'Available Certifications',
        count: 8,
      },
    };
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Grid request={this.state.class} />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
