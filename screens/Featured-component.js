import React, {Component} from 'react';
import {View, Text, SafeAreaView, ScrollView} from 'react-native';
import Grid from '../components/Grid';


export default class Featured extends Component {

  constructor(props) {
    super(props);
    this.state = {
      class: {
        type: 'featured',
        hide_count: false,
        count: 8,
      },
    };
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Grid request={this.state.class} />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
