import React, {Component} from 'react';
import {View, Text, SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Grid from '../components/Grid';

export default class Classes extends Component {
  constructor(props) {
    super(props);
    this.state = {class: {type: 'live', count: 8}};
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Grid request={this.state.class} />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
});
