import React, {Component} from 'react';
import {View, Text, SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Banner from '../components/Banner';
import CustomColors from '../Color/CustomColors';

export default class Home extends Component {
  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Banner />
          <View style={styles.body}>
            <View style={styles.introBanner}>
              <View style={styles.section}>
                <Text style={styles.sectionDescription}>
                  Become a Certified ServiceNow expert.
                </Text>
                <Text style={styles.sectionDescription}>
                  Learn what you need to make an impact and grow your career.
                </Text>
              </View>
            </View>
            <View style={styles.sectionContainer}>
              <Text
                style={{
                  ...styles.sectionDescription,
                  ...styles.infoSection,
                  ...styles.section,
                }}>
                Explore options below to accelerate your learning. Take
                on-demand courses, sign up for live classes taught by ServiceNow
                experts, and choose your learning path
              </Text>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  introBanner: {
    backgroundColor: CustomColors.banner,
    textAlign: 'center',
    color: CustomColors.primary,
  },
  section: {
    margin: 30,
    padding: 15,
  },
  sectionContainer: {
    backgroundColor: CustomColors.primary,
    color: Colors.white,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: CustomColors.primary,
  },
  sectionDescription: {
    fontSize: 22,
    fontWeight: '700',
    color: CustomColors.primary,
    textAlign: 'left',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  infoSection: {
    color: Colors.white,
    fontWeight: 'normal',
    fontSize: 18,
    padding: 10,
    margin: 20,
  },
});
