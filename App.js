/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import React from 'react';
import AppNavigator from './navigation/AppNavigator';
import NLHeader from './components/Header';

const App: () => React$Node = () => {
  return (
    <>
      <NLHeader />
      <AppNavigator />
    </>
  );
};

export default App;
