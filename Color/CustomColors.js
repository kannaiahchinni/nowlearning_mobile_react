'use strict';

export default {
  primary: '#293e40',
  banner: '#b1b1e4',
  secondary: 'rgb(177, 221, 204)',
};
