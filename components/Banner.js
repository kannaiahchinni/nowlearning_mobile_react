'use strict';

import React from 'react';
import {Text, StyleSheet, ImageBackground} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import CustomColors from '../Color/CustomColors';

const Banner = () => (
  <ImageBackground
    accessibilityRole={'image'}
    source={{
      uri:
        'https://www.servicenow.com/content/dam/servicenow-assets/public/en-us/images/company-library/employees/hp-marquee-digital-workflows.png',
    }}
    style={styles.background}
    imageStyle={styles.logo}
  />
);

const styles = StyleSheet.create({
  background: {
    paddingBottom: 40,
    paddingTop: 96,
    paddingHorizontal: 32,
    backgroundColor: CustomColors.banner,
    height: 400,
  },
  logo: {
    overflow: 'visible',
    resizeMode: 'cover',
    /*
     * These negative margins allow the image to be offset similarly across screen sizes and component sizes.
     *
     * The source logo.png image is 512x512px, so as such, these margins attempt to be relative to the
     * source image's size.
     */
    marginLeft: -308,
    marginBottom: -192,
  },
  text: {
    fontSize: 40,
    fontWeight: '600',
    textAlign: 'center',
    color: Colors.black,
  },
});

export default Banner;
