import React, {Component} from 'react';
import {Header, Avatar, Image, Button} from 'react-native-elements';
import CustomColors from '../Color/CustomColors';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class NLHeader extends Component {
  render() {
    return (
      <Header
        leftComponent={<Avatar rounded icon={{name: 'person'}} />}
        centerComponent={
          <Image
            style={{width: 200, height: 24}}
            source={{
              uri:
                'https://nowlearning.service-now.com/images/logos/sn-logo-light-green.png',
            }}
          />
        }
        rightComponent={
          <Button
            icon={{
              name: 'search',
              size: 20,
              color: Colors.white,
            }}
            title=""
            type="clear"
            onPress={() => console.log(this.props.navigation)}
          />
        }
        backgroundColor={CustomColors.primary}
      />
    );
  }
}
