import React, {Component} from 'react';
import {ActivityIndicator, Text, View, FlatList} from 'react-native';
import {API_HOST} from 'react-native-dotenv';
import {Card, Button, Icon} from 'react-native-elements';

export default class Grid extends Component {
  constructor(props) {
    super(props);
    this.state = {isLoading: true};
    console.log(API_HOST);
    console.log(this.props.request);
  }
  componentDidMount() {
    fetch(API_HOST + '/api/x_snc_lxp/lxp_ui_rest_api/device', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.props.request),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson.result.list);
        this.setState({
          isLoading: false,
          liveCourses: responseJson.result.list,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View>
        <FlatList
          data={this.state.liveCourses}
          renderItem={({item}) =>
            <Card
              title={item.name.display_value}>
              <Text style={{marginBottom: 10}}>
                The idea with React Native Elements is more about component
                structure than actual design.
              </Text>
              <Button
                icon={<Icon name="code" color="#ffffff" />}
                buttonStyle={{
                  borderRadius: 0,
                  marginLeft: 0,
                  marginRight: 0,
                  marginBottom: 0,
                }}
                title="VIEW NOW"
              />
            </Card>
          }
          keyExtractor={({sys_id}, index) => sys_id}
        />
      </View>
    );
  }
}
