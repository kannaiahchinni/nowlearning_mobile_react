import React from 'react';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Home from '../screens/Home-component';
import Simulator from '../screens/Simulator-component';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CustomColors from '../Color/CustomColors';
import Certification from '../screens/Certification-component';
import Classes from '../screens/Classes-component';
import Featured from '../screens/Featured-component';

const BottomTabNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        title: 'Home',
        tabBarIcon: ({tinColor}) => (
          <Icon name="home" color={tinColor} size={20} />
        ),
      },
    },
    Classes: {
      screen: Classes,
      navigationOptions: {
        tabBarIcon: ({tinColor}) => (
          <Icon name="library-books" color={tinColor} size={20} />
        ),
      },
    },
    Simulator: {
      screen: Simulator,
      navigationOptions: {
        tabBarIcon: ({tinColor}) => (
          <Icon name="application" color={tinColor} size={20} />
        ),
      },
    },
    Certification: {
      screen: Certification,
      navigationOptions: {
        tabBarIcon: ({tinColor}) => (
          <Icon name="certificate" color={tinColor} size={20} />
        ),
      },
    },
    Featured: {
      screen: Featured,
      navigationOptions: {
        tabBarIcon: ({tinColor}) => (
          <Icon name="apps" color={tinColor} size={20} />
        ),
      },
    },
  },
  {
    initialRouteName: 'Home',
    activeColor: CustomColors.primary,
    inactiveColor: '#226557',
    barStyle: {backgroundColor: CustomColors.secondary},
  },
);

export default BottomTabNavigator;
