<NavigationContainer>
      <NLHeader />
      <Tab.Navigator>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Classes" component={Grid} />
        <Tab.Screen name="Simulators" component={Grid} />
        <Tab.Screen name="Certifications" component={Grid} />
        <Tab.Screen name="Featured" component={Grid} />
      </Tab.Navigator>
    </NavigationContainer>